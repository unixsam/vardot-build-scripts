# HOW TO BUILD FOR NEW PROJECT #

This is an installation guide to build and update (rebuild), Varbase and other distros and sites using Varbase.

### 1. Download the build script from Bitbucket left toolbar (you don't have to clone this repo). ###
```
#!shell

wget https://bitbucket.org/Vardot/vardot-build-scripts/get/7.x-3.x.zip -O ~/vardot_build_script-7.x-3.x.zip
```

### 2. Prepare your project root directory ###
Change `/var/www/` to your preferred web server. Don't forget to change `[project name]` to your project name.
```
#!shell

mkdir -p /var/www/[project name]/docroot
```

### 3. Extract the build script to your new created directory ###
Change `/var/www/` to your preferred web server. Don't forget to change `[project name]` to your project name.
```
#!shell

unzip -j ~/vardot_build_script-7.x-3.x.zip -d /var/www/[project name]/docroot/build
```
Then *optionally* cleanup the original downloaded script. 
```
#!shell

rm ~/vardot_build_script-7.x-3.x.zip
```

### 4. Go into the newly created `build`, make your make file to build the new project website ###
```
#!shell

cd /var/www/[project name]/docroot/build
cp example.make [project name].make
```

### 5. Run the build script ###
```
#!shell

./build.sh
```

### 6. Follow the wizard ###
You will need the most recent Drush version. See [this snippet](https://bitbucket.org/snippets/Vardot/8rGL#1%29%20Install%20nodeJS%2C%20npm%2C%20less%2C%20sass%20and%20Drush-18).